function executeTasks1(){
  var inputPhase = document.getElementById('inputText').value
  inputPhase = trimSpaces(inputPhase);
  console.log(inputPhase);
  var result = extractedWordsA(trimSpaces(inputPhase));
  document.getElementById('extractedWords').value = '[ '+result+' ]';
  result = extractedWordsB(inputPhase);
  document.getElementById('extractedReverseWords').value = '[ '+result+' ]';
  result = extractedWordsC(inputPhase);
  document.getElementById('sortedWords').value = '[ '+result+' ]';
  result = extractedWordsDa(inputPhase);
  document.getElementById('divideEqualNumOfLetters1').value = '[ '+result[0]+' ]';
  document.getElementById('divideEqualNumOfLetters2').value = '[ '+result[1]+' ]';
  result = extractedWordsDb(inputPhase);
  document.getElementById('divideEqualNumOfWords1').value = '[ '+result[0]+' ]';
  document.getElementById('divideEqualNumOfWords2').value = '[ '+result[1]+' ]';
}

function executeTasks2(){
  var inputPhase = document.getElementById('inputArray').value
  inputPhase = parseStringToArray(inputPhase);
  var result = getArrayParameters(inputPhase);
  document.getElementById('smallestValue').value = 'Smallest value: '+result[0];
  document.getElementById('BiggestValue').value = 'Biggest value: '+result[1];
  document.getElementById('avarage').value = 'Avarage: '+result[2];
  document.getElementById('variance').value = 'Variance: '+result[3];
}


function executeTasks3(){
  var inputPhase = document.getElementById('inputList').value
  inputPhase = parseStringToArray(inputPhase);
  var inputValue = document.getElementById('inputArg').value
  var result = countBiggest(inputPhase, inputValue);
  document.getElementById('howManyBiggest').value = result;
}

function executeTasks12(){
  var expression = document.getElementById('inputExpression').value
  var result = changeToRPNNotation(expression);
  document.getElementById('RPNOutput').value = result;
  result = countRPNExpression(result);
  console.log("execute - " + result);
  document.getElementById('result').innerHTML = result;
}

function parseStringToArray(array){
    array = trimSpaces(array);
    return array.split(/[\s,]+/).map(function(item) {
        return parseFloat(item, 10);
    });
}

function trimSpaces(string){
    string = string.replace(/\s\s+/g, ' ');
    return string;
}
