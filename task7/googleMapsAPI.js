function initMap(lng=17.0640218, lat=51.1144556) {
    // Create a map object and specify the DOM element for display.
    var localization = {lng:lng, lat:lat};
    var map = new google.maps.Map(document.getElementById('map'), {
      center: localization,
      zoom: 4,
      zoomControl: true,
      mapTypeControl: true,
    });
    google.maps.event.addListener(map, 'click', function( event ){
    	setLocation(event.latLng.lat(), event.latLng.lng());
	});
}
