function countBiggest(array, number){
  var counter = 0;
  for (var i = 0 ; i < array.length; i++){
    if (array[i] > number){
      counter++;
    }
  }
  return counter;
}
