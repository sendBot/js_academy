var tempLiterals = [];
var listOfLiterals = {};

function changeToRPNNotation(exp) {
  var operators = ["+", "-", "*", "/", "^", "(", ")", "="];
  var priority = {
    '+': 1,
    '-': 1,
    '*': 2,
    '/': 2,
    '(': 0,
    '^': 3
  };
  var output = [];
  var stack = [];
  exp = exp.replace(/\s\s+/g, " ").split(' ');
  checkIfLiteral(exp); // check if expressions starts with <symbol> <=>

  for (var i = 0; i < exp.length; i++) { // for each of element from expression loop
    var expressionElement = exp[i];
    var ifBracketOpen = false;

    if (!parseFloat(expressionElement) && !expressionElement.match(/[a-z]/i)) { //is not a number
      if (operators.indexOf(expressionElement) > -1) { //check if is allowed
        if (expressionElement === "(") {
          ifBracketOpen = true;
        }
        if (expressionElement === ')') {
          ifBracketOpen = false;
          var stackElement;
          do {
            stackElement = stack.pop();
            output.push(stackElement);
          } while (stackElement !== '(');
          output.pop(); // remove '(' from output
        }
        if (ifBracketOpen === false &&
          priority[stack[stack.length - 1]] >= priority[expressionElement]) { //check if last element from stack have a highest or equal priority than current
          output.push(stack.pop());
          stack.push(expressionElement);
        }
        else {
          if (expressionElement !== ')') {
            stack.push(expressionElement);
          }
        }
      }
    }
    else { //is a number
      output.push(expressionElement);
    }
  }

  for (var j = 0; j < stack.length ; j++) { //redirect to output rest of stack
    output.push(stack.pop());
  }
  return output.join(' ');
}

function countRPNExpression(expression) {
  expression = expression.replace(/\s\s+/g, " ").split(' ');
  var stack = [];
  for (var i = 0; i <= expression.length-1; i++) {
    var exp = expression[i];
    exp = resolveLiteral(exp); //check if current element is literal and if it is already declared
    if (parseFloat(exp)) {
      stack.push(exp);
    }
    else {
      stack.push(doOperations(stack.pop(), stack.pop(), exp)); // resolve operations from end of stack
    }
  }
  var result = stack.pop();
  if(tempLiterals[0]){ // if temp literal exist
    listOfLiterals[tempLiterals[0]] = result; //create a new literal in list with temp literal and current result
    tempLiterals.shift(0,1); // remove literal from list
  }
  return result;
}

function doOperations(num1, num2, operator) {
  num1 = parseFloat(num1);
  num2 = parseFloat(num2);
  switch (operator) {
  case '+':
    return num2 + num1;
  case '-':
    return num2 - num1;
  case '*':
    return num2 * num1;
  case '/':
    return num2 / num1;
  case '^':
    return Math.pow(num2, num1);
  }
}

function checkIfLiteral(exp){
  var firstExpression = exp[0];
  if(!parseFloat(firstExpression) && firstExpression.match(/[a-z]/i) && exp[1] === '='){ // check if first expression is literal and second is '=''
    tempLiterals.push(firstExpression); // put literal to temp list to first execute counting
    exp.splice(0, 2); // remove from expression literaz and =
  }
  return exp;
}

function resolveLiteral(symbol) {
  if(symbol.match(/[a-z]/i)){ //if literal is from avaliable range
    var temp = listOfLiterals[symbol];
    return temp;
  }
  else{
    return symbol; // important to return symbol
  }
}

//countRPNExpression('2 7 + 3 / 14 3 - 4 * + 2 /');
//changeToRPNNotation('( ( 5 - 2 ) ^ ( 3 + 1 ) / ( 2 + 1 ) + 12 ) * 21');
//console.log("myres = " + changeToRPNNotation('z + 5'));
