function getDataForLocation(location="Wroclaw"){
  var html = "http://apidev.accuweather.com/locations/v1/search?q="+location+"&apikey=hoArfRosT1215";
  $.getJSON(html, function(result) {
    console.log(result);
    var city = result[0].LocalizedName;
    var country = result[0].AdministrativeArea.CountryID;
    var key = result[0].Key;
    
    //$("#data").html("latitude: " + lon + "<br>longitude: " + lat);
    $("#location").html( city +",  "+country);
    var html = "http://apidev.accuweather.com/currentconditions/v1/"+key+".json?language=en&apikey=hoArfRosT1215"
    
    $.getJSON(html, function(result) {
      displayWeatherDetails(result);
    });
  });
}

function displayWeatherDetails(result){
  var icon = result["0"].WeatherIcon;
  if (icon < 10){
    icon = "0"+icon;
  }
  var iconID = "https://apidev.accuweather.com/developers/media/default/weathericons/"+icon+"-s.png";
  
  $("#description").html('<b>('+result[0].WeatherText+')</b>');
  $("#weather-temp").html('<span id="temparature">'+result[0].Temperature.Metric.Value +'</span>°<span id="tempUnit" onclick="changeUnit()">'+ result[0].Temperature.Metric.Unit)+'</span>';
  $("#iconImg").attr("src", iconID);
}

function changeUnit(){
    $("#weather-temp").fadeOut(1000, function(){
      var currentUnit = $("#tempUnit").text();
      var currentTemp = parseFloat($("#temparature").text());
      if ( currentUnit == "C"){
        document.getElementById("tempUnit").innerHTML = "F";
        var temp = currentTemp * 9/5+32 ;
      }
      else {
        document.getElementById("tempUnit").innerHTML = "C";
        var temp = (currentTemp - 32)*5/9;
      }
      temp = Math.round(temp * 100) / 100
      $("#temparature").html(temp);
    });
    $("#weather-temp").fadeIn(1000);
  };

function findLocation(){
  var inputLocation = document.getElementById('locationInput').value;
  $(':input').val('');
  inputLocation.split(' ').join('&');
  getDataForLocation(inputLocation);
}

function searchMode(){
  $(document.body).click(function(e){
   if(e.target.id !== 'searchField' && e.target.id !== 'locationInput'){
      $("#searchField").animate({width:450}, 500);
    }
    else {
      $("#searchField").animate({width:'100%'}, 500);
    }
  });
}

function getLocation() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var lon =  position.coords.latitude;
      var lat = position.coords.longitude;
      setLocation(lat, lon);
    });
  }
} 

function setLocation(lat, lon){
  var html = "http://api.accuweather.com/locations/v1/cities/geoposition/search.json?q="+lon+",%20"+lat+"&apikey=ff1b463d98fb47af848ea2843ec5c925";
      
  $.getJSON(html, function(result) {
    console.log(result);
    var locations = result.LocalizedName;
    getDataForLocation(locations);
  });
}