function drawCoordinateSystem() {
    var canvas = document.getElementById("myCanvas");
    var ctx = canvas.getContext("2d");
    ctx.moveTo(0, (canvas.width/2));
    ctx.lineTo(canvas.width, (canvas.width/2));
    ctx.stroke();

    ctx.moveTo((canvas.width/2), 0);
    ctx.lineTo((canvas.width/2), canvas.width);
    ctx.stroke();
}

function drawPoint(){
	var canvas = document.getElementById("myCanvas");
	var width = canvas.width;
	var xVal = parseInt(document.getElementById("xCoord").value) +(width/2);
	var yVal = (width/2) - parseInt(document.getElementById("yCoord").value);

    var ctx = canvas.getContext("2d");
	ctx.beginPath();
	ctx.arc(xVal,yVal,2,0,2*Math.PI);
	ctx.fill();
	ctx.stroke();
}

