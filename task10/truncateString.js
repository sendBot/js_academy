function truncateString(string, amount){
  string = trimSpaces(string).split(' ');
  var result = string.slice(0,amount);
  return result.join(' ');
} 

function trimSpaces(string){
    string = string.replace(/\s\s+/g, ' ');
    return string;
}

truncateString("ala ma kota a kot ma gruźlicę", 4);
