function countHowManySubstring(string, substring){
	string = string.toLowerCase();
  	var re = new RegExp(substring,'g');
	var count = (string.match(re) || []).length;
	return count;
}

countHowManySubstring("Ala alicja alunia", "al");