function getArrayParameters(array){
  var biggest = getBiggestValue(array);
  var smallest = getSmallestValue(array);
  var avarage = Math.round(countAvarage(array) * 100 )/ 100;
  var variance = Math.round(countVariance(array, avarage) * 100 )/ 100;
  return [smallest, biggest, avarage, variance];
}

function getBiggestValue(array){
  array = array.sort(function(a,b){
    return a - b ;
  });
  return array[array.length-1];
}

function getSmallestValue(array){
  array = array.sort(function(a,b){
    return a - b ;
  });
  return array[0];
}

function countAvarage(array){
  var amount = array.length;
  var sum = 0;
  
  for (var i = 0 ; i < array.length; i++){
    sum += array[i];
  }
  console.log("sum: "+sum);
  console.log("amount "+amount);
  return sum/amount;
}

function countVariance(array, avarage){
  var amount = array.length;
  var sum = 0;
  
  for (var i = 0 ; i < array.length; i++){
    sum += Math.pow((array[i] - avarage),2);
  }
  return sum/amount;
}

