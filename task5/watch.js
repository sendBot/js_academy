
$(document).ready(function() {
  startTime();
});
  
function displayFormat(i) {
  if (i < 10) {
    i = "0" + i;
  }
  return i;
}

function startTime() {
  var today = new Date();
  var h = today.getHours();
  var m = today.getMinutes();
  var s = today.getSeconds();
  var dd = today.getDate();
  var mm = today.getMonth()+1; //January is 0!
  var yyyy = today.getFullYear();
  // add a zero in front of numbers<10
  m = displayFormat(m);
  s = displayFormat(s);
  dd = displayFormat(dd);
  mm = displayFormat(mm);

  var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
  var day = days[ today.getDay() ];

  document.getElementById('hour_box').innerHTML = h + ":" + m + ":" + s;
  document.getElementById('date_box').innerHTML = dd + '-' + mm + '-' + yyyy+'<span style="font-size:25px;">('+day+')</span>';
  t = setTimeout(function() {
    startTime()
    if (m%10 === 0 && s%30 === 0){
      changePageColors();
    }
  }, 1000);
}

function changePageColors(){
  var arrayNum = [
      "#73D08D",
      "#9E9160",
      "#45457B",
      "BAAF08",
      "#703E7B",
      "#348F72"
    ];
    var colorr = arrayNum[Math.floor(Math.random() * arrayNum.length)];
      $("body").css("background-color", colorr);
}