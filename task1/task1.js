<!-- 
//Task1 A
-->
function extractedWordsA(sentence){
  var wordsArray = [];
  wordsArray = sentence.split(" ");

  return wordsArray;
}

<!-- 
//Task1 B
-->
function extractedWordsB(sentence){
  wordsArray = extractedWordsA(sentence)
    .reverse();

  return wordsArray;
}

<!-- 
//Task1 C
-->
function extractedWordsC(sentence){
  wordsArray = extractedWordsA(sentence);
  //<!-- Case insensitive sorting -->
  wordsArray.sort(function(a,b){
    return a.toLowerCase().localeCompare(b.toLowerCase());
  });
  wordsArray.reverse();
  
  return wordsArray;
}

<!-- 
//Task1 D A
-->
function extractedWordsDa(sentence){
  var wordsArray = [];
  var result1 = [];
  var result2 = [];
  wordsArray = extractedWordsA(sentence);
	
  //<!-- word length sorting -->
  wordsArray.sort(function(a,b){
    return a.length - b.length;
  });
  divideWords(wordsArray, result1, result2);
  return [result1, result2];
}

function divideWords(arr, result1, result2){
  if (arr.length === 0){
  	return result1, result2;
  }
  else if (arr.length === 1){
  	return result1.push(arr[0]);
  }
  else{
    while(arr[0] !== undefined){
      var index = arr.indexOf(arr[arr.length-1]);
      result1.push(arr[index]);
      arr.splice(index,1);
    
      while (checkLen(result1) > checkLen(result2)){
        if (arr[0] === undefined){break;}
        result2.push(arr[0]);
        arr.splice(0,1);
      }
    }
  }
}

function checkLen(arr){
  var counter = 0;
  	for (var k in arr){
      if (arr[k].length !== 0 ){
        counter += arr[k].length;
      }
  	}
  return counter;
}

<!-- 
//Task1 D A
-->
function extractedWordsDb(sentence){
  var wordsArray = [];
  var result1 = [];
  var result2 = [];
  wordsArray = extractedWordsA(sentence);
  var arrLen = wordsArray.length;

  if (wordsArray.length === 0 || wordsArray.length === 1){
    return result1, result2;
  }
  else{
    
    if (wordsArray.length%2 !== 0){
      arrLen--;
    }
    for(var i = 0 ; i < arrLen; i++){
      if(i%2 === 0 ){
        result1.push(wordsArray[i]);
      }
      else{
        result2.push(wordsArray[i]);
      }
    }
  }
  return [result1, result2];
}
